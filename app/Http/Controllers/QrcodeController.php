<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\QrcodeModel;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\QrcodeExport;
use DataTables;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class QrcodeController extends Controller
{
    public function index()
    {
        $qrcode = QrcodeModel::all();
        // return response()->json($qrcode);
        return view("cms.qrcode.index", compact("qrcode"));
    }

    public function qrjson()
    {
         $qrcode = QrcodeModel::orderBy('created_at', 'asc')->get();
        //  dd($qrcode)
        return DataTables::of($qrcode)
     
         ->addColumn('qrcode', function ($user) {
                $qr = QrCode::size(150)->generate(url('/').'/generate/'.$user->id);
                return $qr;
            })
            
            ->addColumn('print', function($print)
            {
             return '<a class="btnPrint" href="/qrcode/'.$print->id.'" target="popup"><button class="btn btn-primary btn-sm">Print QR</button></a>';
            })

            ->addColumn('tanggal', function($tgl){
                return $tgl->created_at->format('Y-m-d');
            })
        
        ->rawColumns(['print'])
        ->make(true);
          
        
    }

    // public function generateQr($id)
    // {   
    //     $qr = QrcodeModel::find($id);
    //     $gen = QrCode::generate($qr);

    //     return Datatables::of($gen)->make(true);
    //     // dd($qr);
    // }

    public function create()
    {
        return view("cms.qrcode.create");
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $qrcode = new QrcodeModel();
       $qrcode->nama_p3mi = $request->nama_p3mi;
        $qrcode->nama_pmi = $request->nama_pmi;
         $qrcode->no_paspor = $request->no_paspor;
        $qrcode->negara_tujuan = $request->negara_tujuan;
       $qrcode->jumlah_hutang = $request->jumlah_hutang;
        $qrcode->lembaga_kredit = $request->lembaga_kredit;
        $qrcode->save();

        return redirect()->back()->with("msg", 'Sukses');
    }

    public function generateData($id)
    {
        // dd($id);
        $qr = QrcodeModel::find($id);
        // dd($qr->nama);
        return view("front.generate", compact("qr"));
    }

    public function qrcode($id)
    {
        $qr = QrcodeModel::find($id);
        // dd($id);
        // dd($qr);
        return view("front.qrcode", compact("qr"));
    }

    public function Exportindex(){
        return view("cms.export.index");
    }

    public function export(Request $request)
    {
        // dd($request->all());
        $tahun = $request->tahun;
        $bulan = $request->bulan;
        $export = new QrcodeExport($tahun, $bulan);
        return Excel::download($export, 'qrcodedata.xlsx');
    }

}
