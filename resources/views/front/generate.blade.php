<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    {{-- <div class="card" style="width: 18rem;">
  <div class="card-header">
    <strong>Nama P3MI</strong>
  </div>
  <div class="card-header">
    {{ $qr->nama_p3mi }}
  </div>
  <div class="card-header">
    <strong>Nama PMI</strong>
  </div>
  <div class="card-header">
    {{ $qr->nama_pmi }}
  </div>
  <div class="card-header">
    <strong>Negara Tujuan</strong>
  </div>
  <div class="card-header">
    {{ $qr->negara_tujuan }}
  </div>
  <div class="card-header">
    <strong>Jumlah Hutang</strong>
  </div>
  <div class="card-header">
    Rp. {{ number_format($qr->jumlah_hutang, 0, '.', '.') }}
  </div>
  <div class="card-header">
    <strong>Lembaga Kredit</strong>
  </div>
  <div class="card-header">
    {{ $qr->lembaga_kredit }}
  </div>

  
  
  
  
  
  
</div> --}}

<table class="table table-dark table-bordered">
  
  <tbody>
    <tr>
    
      <td>Nama P3MI</td>
      <td>{{ $qr->nama_p3mi }}</td>
    </tr>
    <tr>
      <td>Nama PMI</td>
      <td>{{ $qr->nama_pmi }}</td>
      
    </tr>
      <tr>
      <td>No Paspor</td>
      <td>{{ $qr->no_paspor }}</td>
      
    </tr>
      <tr>
      <td>Negara Tujuan</td>
      <td>{{ $qr->negara_tujuan }}</td>
      
    </tr>
      <tr>
      <td>Jumlah Hutang</td>
      <td>Rp. {{ number_format($qr->jumlah_hutang, 0, '.', '.') }}</td>
      
    </tr>
      <tr>
      <td>Lembaga Kredit</td>
      <td>{{ $qr->lembaga_kredit }}</td>
      
    </tr>
    
  </tbody>
</table>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>