@extends('cms.layouts.app')

@section('heading')
Data
@endsection

@section('content')
<div class="row g-gs">
  <div class="col-sm-6">
    <div class="card card-bordered">
        <div class="card-inner">
            <div class="card-title-group align-start mb-2">
                <div class="card-title"><h6 class="title">Data Koperasi</h6></div>
                <div class="card-tools"><em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total active subscription"></em></div>
            </div>
            <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                <div class="nk-sale-data">
                    <span class="amount">{{$total}} data</span>
                    <span class="sub-title">
                        {{-- <span class="change down text-danger"><em class="icon ni ni-arrow-long-down"></em>1.93%</span>since last month --}}
                    </span>
                </div>
                <div class="nk-sales-ck">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand"><div class=""></div></div>
                        <div class="chartjs-size-monitor-shrink"><div class=""></div></div>
                    </div>
                    <canvas class="sales-bar-chart chartjs-render-monitor" id="activeSubscription" style="display: block; width: 193px; height: 56px;" width="193" height="56"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

  @endsection