@extends('cms.layouts.app')

@section('css')

<style>
@media print {
   /* .else {
       display: none;
    } */
    .gaya {
       
      width:500px;
      background:#000;
    }
 
}
@page { margin: 0 auto; background: #036;}
table.dataTable tbody tr{
  background:none !important;
}

</style>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
<script src="{{ url('/').'/js/' }}jquery.printPage.js"></script>

  <script>  
  $(document).ready(function() {
    $(".btnPrint").printPage();
  });
  </script>
@endsection

@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8" style="background:#000 !important; background-repeat:no-repeat !important; background-size: cover !important;
    background-position: center !important;">
      <div class="container-fluid">
        <div class="header-body">
        
       
        
          
         {{-- <button class="btn">Trashed({{ $trash }})</button> --}}
        </div>
        <form action="qrcode/export" method="POST">
          {{ csrf_field() }}
          <div class="container">
              <div class="row">
              
              <div class="col-md-4">        
                  <select class="form-control" name="bulan" >
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>

              </div>
              <div class="col-md-4">        
                  <select class="form-control" name="tahun">
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                  </select>

              </div>
                {{-- <a href=""> <button class="btn btn-success">Export Data</button></a> --}}
                <input type="submit" class="btn btn-success" value="Export Data">
              
              </div>
           </div>
        </form>
      </div>
    </div>
    
    
    <!-- Dark table -->
    <div class="container-fluid mt--7">
      
      <div class="row ">
        
        {{-- <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-white mb-0">Menu List</h3>
            </div>
            <div class="table-responsive">
              
            
            </div>
          </div>
        </div> --}}
      </div>
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          
        </div>
      </footer>
    </div>
    
    

@endsection