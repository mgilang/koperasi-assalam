@extends('cms.layouts.app')

@section('css')

<style>

div.dataTables_wrapper div.dataTables_processing
{
  background: #8dca90 !important;
        color: white !important;
        height: 60px !important;
        z-index: 99999999;
        top:100px;
}
@media print {
   /* .else {
       display: none;
    } */
    .gaya {
       
      width:500px;
      background:#000;
    }
 
}
@page { margin: 0 auto; background: #036;}
table.dataTable tbody tr{
  background:none !important;
}

</style>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

{{-- <script src="{{ url('/').'/js/' }}jquery.printPage.js"></script> --}}


@endsection

@section('content')

@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
<div class="row">
  <div class="col-6">
     <a href="cms/qrcode/add"> <button class="btn btn-primary btn-md">New Data</button></a>
  </div>
</div>

<div class="nk-block nk-block-lg">
    <div class="nk-block-head">
        <div class="nk-block-head-content">
            {{-- <h4 class="nk-block-title">Data Table</h4> --}}
            <div class="nk-block-des">
                {{-- <p>Using the most basic table markup, here’s how <code class="code-class">.table</code> based tables look by default.</p> --}}
            </div>
        </div>
    </div>
    {{-- {!! QrCode::size(150)->generate("https://koperasi.didev.id/generate/50") !!} --}}
    <div class="card">
        <div class="card-inner">
            <table class="table datatable" id="usertable">
              <thead class="thead-dark"> 
                  <tr>
              
                    <th scope="col">Nama P3MI</th>
                    <th scope="col">Nama PMI</th>
                    <th scope="col">No Paspor</th>
                    <th scope="col">Negara Tujuan</th>
                    <th scope="col">Jumlah Hutang</th>
                    <th scope="col" widt="100">Lembaga Kredit</th>
                    <th scope="col">QR CODE</th>
                    <th scope="col">Print</th>
                    <th scope="col">Tanggal</th>
                    {{-- <th scope="col">Action</th> --}}
                    {{-- <th scope="col">Completion</th>
                    <th scope="col"></th> --}}
                  </tr>
                </thead>
                
              </table>
        </div>
    </div>
</div>
{{-- 
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8" >
      <div class="container-fluid">
        <div class="header-body">
        
        
       
        </div>
      </div>
    </div>
    

    <div class="">
      
      <div class="row ">
        
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-white mb-0">Menu List</h3>
            </div>
            <div class="table-responsive">
              <table class="datatable-init table" id="usertable">
                <thead class="thead-dark"> 
                  <tr>
              
                    <th scope="col">Nama P3MI</th>
                    <th scope="col">Nama PMI</th>
                    <th scope="col">No Paspor</th>
                    <th scope="col">Negara Tujuan</th>
                    <th scope="col">Jumlah Hutang</th>
                    <th scope="col">Lembaga Kredit</th>
                    <th scope="col">QR CODE</th>
                    <th scope="col">Action</th>
                
                  </tr>
                </thead>
                
              </table>
            
            </div>
          </div>
        </div>
      </div>
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          
        </div>
      </footer>
    </div> --}}
    
    

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.0/jQuery.print.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <script src="{{ asset('admin/js/jquery.printPage.js') }}"></script>
<script>  
  $(document).ready(function() {
    $(".btnPrint").printPage();
  });
  </script>
<script>
  // function myFunction() {
    
  //   // var ele = $(this);              
  //   var printContents = document.getElementById("printqrcode").innerHTML;
  //   // console.log(printContents);
  //   // var printContents =  ele.parents("td").find(".printqrcode").innerHTML;
  //   var originalContents = document.body.innerHTML;
    


  //   document.body.innerHTML = printContents;
    
  //   window.print();

  //   document.body.innerHTML = originalContents;
  // }
  </script>
  
  <script>
        $(document).ready( function () {
            $('#usertable').DataTable({
              "paging":true,
               "searching": true,
              "ordering": true,
              "pageLength": 5,
              "order": [[ 7, "desc" ]],
                "processing": true,
                "serverSide": false,
                "ajax":{
                    "url":"{{ url('/cms/qrcode/json') }}",
                    "type": "GET",
                    // "dataSrc": "",
                    
                },
                "columns":[
                  // { "data": "id"},
                  {"data":"nama_p3mi"},
                  {"data": "nama_pmi"},
                  {"data": "no_paspor"},
                  {"data": "negara_tujuan"},
                  {"data": "jumlah_hutang"},
                  {"data": "lembaga_kredit"},
                  {"data": "qrcode"},
                  {"data": "print"},
                  {"data": "tanggal"} 
                  
                ],
                "columnDefs": [
                  { "width": "5%", "targets": 5 }
                ],
                language: {
                    "processing": "Loading. Mohon Tunggu..."
                },
            });

        } );
</script>
@endsection

