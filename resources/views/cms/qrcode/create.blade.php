@extends('cms.layouts.app')

@section('content')


<div class="row">
  <div class="col-6">
    @if (session('msg'))
  <div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  </button>{{ session('msg') }}</div>
  @endif
  </div>
</div>


<div class="row">
  <div class="col-lg-6">
    <div class="card h-100">
        <div class="card-inner">
            <div class="card-head"><h5 class="card-title">Input Data</h5></div>
            <form method="POST" action="add/submit" enctype="multipart/form-data">
              {{ csrf_field() }}
                <div class="form-group"><label class="form-label" for="cf-full-name">Nama PTKIS / P3MI</label>
                  <input type="text" id="menu" name="nama_p3mi" class="form-control form-control-alternative" required>
                </div>
                <div class="form-group"><label class="form-label" for="cf-email-address">Nama PMI</label>
                  <input type="text" id="menu" name="nama_pmi" class="form-control form-control-alternative" required>
                </div>
                <div class="form-group"><label class="form-label" for="cf-phone-no">No Paspor</label>
                  <input type="text" id="menu" name="no_paspor" class="form-control form-control-alternative" required>
                </div>
                <div class="form-group"><label class="form-label" for="cf-subject">Negara Tujuan</label>
                  <input type="text" id="menu" name="negara_tujuan" class="form-control form-control-alternative" required>
                </div>
                <div class="form-group"><label class="form-label" for="cf-subject">Jumlah Hutang</label>
                  <input type="number" id="price" name="jumlah_hutang" class="form-control form-control-alternative" required>
                </div>
                <div class="form-group"><label class="form-label" for="cf-subject">Lembaga Kredit</label>
                  <input type="text" id="menu" name="lembaga_kredit" class="form-control form-control-alternative" required>
                </div>
               
                <div class="form-group"><button type="submit" class="btn btn-lg btn-primary">Submit</button></div>
            </form>
        </div>
    </div>
</div>
</div>
    
  

    <!-- Dark table -->
    {{-- <div class="container-fluid mt--7">
      
      <div class="row ">
        
        <div class="col-xl-8 order-xl-1">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">New Data</h3>
                </div>
                <div class="col-4 text-right">
           
                </div>
              </div>
            </div>
            <div class="card-body">
              <form method="POST" action="add/submit" enctype="multipart/form-data">
                {{ csrf_field() }}
               
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group focused">
                        <label class="form-control-label" for="input-username">Nama PTKIS / P3MI</label>
                      
                        
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group focused">
                        <label class="form-control-label" for="input-username">Nama PMI</label>
                       
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group focused">
                        <label class="form-control-label" for="input-username"></label>
                        
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group focused">
                        <label class="form-control-label" for="input-username"></label>
                        
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email"></label>
                        
                      </div>
                    </div>
                     <div class="col-lg-12">
                      <div class="form-group focused">
                        <label class="form-control-label" for="input-username"></label>
                        
                      </div>
                    </div>
                  </div>
                

                   <a href="item/add"> <button class="btn btn btn-info">Submit</button></a>
                </div>
              
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> --}}

@endsection