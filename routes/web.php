<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix'=>'cms', 'middleware'=>'auth'], function () {
    // Route::get('/cms', 'DashboardController@index');
      Route::get('/', function(){
      

        return redirect('cms/dashboard'); 
        
    });  

    Route::get('dashboard', 'DashboardController@index'); 
    Route::get('qrcode', 'QrcodeController@index'); 
    Route::get('export', 'QrcodeController@Exportindex'); 
    Route::get('qrcode/add', 'QrcodeController@create'); 
    Route::post('add/submit', 'QrcodeController@store'); 
    Route::post('qrcode/export', 'QrcodeController@export');
    route::get('qrcode/json', 'QrcodeController@qrjson');
    
    route::get('generateqr/{id}','QrcodeController@generateQr');

});

Route::get('/', 'HomeController@index');
Route::get('generate/{id}','QrcodeController@generateData');
Route::get('qrcode/{id}','QrcodeController@qrcode');
